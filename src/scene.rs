use crate::{object::Object, render::Render};

pub struct Scene<'a> {
    pub objects: Vec<Object<'a>>,
}

impl<'a> Scene<'a> {
    pub fn new() -> Self {
        Scene {
            objects: vec![],
        }
    }

    pub fn add_object(&mut self, object: Object<'a>) {
        self.objects.push(object);
    }
}
