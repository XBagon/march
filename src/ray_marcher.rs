use crate::{object::Object, render::Render, scene::Scene, types::*};

#[derive(Debug, Clone)]
pub struct Ray {
    pub origin: Point3,
    pub direction: Vec3,
}

impl Ray {
    pub fn new(origin: Point3, direction: Vec3) -> Self {
        Ray {
            origin,
            direction,
        }
    }
}

#[derive(Clone)]
pub struct RayMarcher {
    max_steps: usize,
    epsilon: f64,
}

static DEFAULT_STEPS: usize = 200;
static DEFAULT_EPSILON: f64 = 1e-6;

impl Default for RayMarcher {
    fn default() -> Self {
        RayMarcher {
            max_steps: DEFAULT_STEPS,
            epsilon: DEFAULT_EPSILON,
        }
    }
}

pub struct MarchCollision<'a> {
    pub object: &'a Object<'a>,
    pub at_point: Point3,
    pub normal: Vec3,
}

pub type MarchResult<'a> = Option<MarchCollision<'a>>;

impl RayMarcher {
    pub fn new(max_steps: usize, epsilon: f64) -> Self {
        RayMarcher {
            max_steps,
            epsilon,
        }
    }
}

impl RayMarcher {
    pub fn march<'a>(&'a self, ray: &Ray, scene: &'a Scene) -> MarchResult<'a> {
        let mut t = 0.0;
        for _ in 0..self.max_steps {
            let point = ray.origin + ray.direction * t;
            let (obj, dist) = self.closest(point, scene);
            if dist < self.epsilon {
                let object = obj.unwrap();
                let normal = Self::compute_normal(object, point.clone());
                let collision = MarchCollision {
                    object,
                    at_point: point,
                    normal,
                };
                return Some(collision);
            }
            t += dist;
        }
        None
    }

    fn compute_normal(obj: &Object, at_point: Point3) -> Vec3 {
        const DELTA: f64 = 1e-5;
        let base_vectors = nalgebra::Matrix3::identity();
        let mut gradient = Vec3::new(0.0, 0.0, 0.0);

        for (i, v) in base_vectors.column_iter().enumerate() {
            let (from, to) = (at_point - DELTA * v, at_point + DELTA * v);
            gradient[i] += obj.render.distance(to) - obj.render.distance(from);
        }

        return gradient.normalize();
    }

    fn closest<'a>(&'a self, point: Point3, scene: &'a Scene) -> (Option<&'a Object>, f64) {
        scene
            .objects
            .iter()
            .map(|o| (Some(o), o.render.distance(point)))
            .min_by(|(_, x), (_, y)| x.partial_cmp(y).unwrap_or(std::cmp::Ordering::Equal))
            .unwrap_or((None, std::f64::INFINITY))
    }
}
