use crate::{render::Render, shader::Shader, types::*};

pub struct SdfCube {
    pub center: Point3,
    pub rotation: Rotation3,
    pub size: f64,
}

impl SdfCube {
    pub fn new(center: Point3, rotation: Rotation3, size: f64) -> Self {
        SdfCube {
            center,
            rotation,
            size,
        }
    }
}

impl Render for SdfCube {
    fn distance(&self, point: Point3) -> f64 {
        (self.rotation * (point - self.center)).amax() - self.size
    }
}
