use crate::{render::Render, shader::Shader, types::*};

pub struct Sphere {
    pub center: Point3,
    pub radius: f64,
}

impl Sphere {
    pub fn new(center: Point3, radius: f64) -> Self {
        Sphere {
            center,
            radius,
        }
    }
}

impl Render for Sphere {
    fn distance(&self, point: Point3) -> f64 {
        nalgebra::distance(&self.center, &point) - self.radius
    }
}
