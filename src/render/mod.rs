use crate::{shader::Shader, types::*};

pub mod sdf_cube;
pub mod sphere;
pub mod subtraction;

pub trait Render: Sync {
    fn distance(&self, point: Point3) -> f64;
}
