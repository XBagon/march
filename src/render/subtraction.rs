use crate::{render::Render, shader::Shader, types::*};

pub struct Subtraction<'a> {
    minuend: Box<dyn Render + 'a>,
    subtrahend: Box<dyn Render + 'a>,
}

impl<'a> Subtraction<'a> {
    pub fn new(minuend: impl Render + 'a, subtrahend: impl Render + 'a) -> Self {
        Subtraction {
            minuend: Box::new(minuend),
            subtrahend: Box::new(subtrahend),
        }
    }
}

impl<'a> Render for Subtraction<'a> {
    fn distance(&self, point: Point3) -> f64 {
        self.minuend.distance(point).max(-self.subtrahend.distance(point))
    }
}
