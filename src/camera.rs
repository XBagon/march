use crate::{Isometry3, Perspective3};

pub struct Camera {
    pub isometry: Isometry3,
    fovy: f64, 
    znear: f64, 
    zfar: f64,
    resolution: Resolution,
}

#[derive(Clone, Copy)]
pub struct Resolution {
    pub x: u32,
    pub y: u32,
}

impl Resolution {
    pub fn new(x: u32, y: u32) -> Self {
        Resolution {
            x,
            y,
        }
    }
}

impl Camera {
    pub fn new(isometry: Isometry3, fovy: f64, znear: f64, zfar: f64, resolution: Resolution) -> Self {
        Camera {
            isometry,
            fovy,
            znear,
            zfar,
            resolution,
        }
    }

    pub fn compute_perspective(&self) -> Perspective3 {
        Perspective3::new (
            self.resolution.x as f64 / self.resolution.y as f64,
            self.fovy.to_radians(),
            self.znear, 
            self.zfar,
        )
    }

    pub fn set_resolution(&mut self, x: u32, y: u32) {
        self.resolution.x = x;
        self.resolution.y = y;
    }

    pub fn get_resolution(&self) -> Resolution {
        self.resolution
    }

    pub fn get_isometry(&self) -> Isometry3 {
        self.isometry
    }

    pub fn get_isometry_mut(&mut self) -> &mut Isometry3 {
        &mut self.isometry
    }
}
