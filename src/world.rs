use crate::{Camera, Scene};

pub struct World<'a> {
    scene: Scene<'a>,
    camera: Camera,
}

impl<'a> World<'a> {
    pub fn new(scene: Scene<'a>, camera: Camera) -> Self {
        World {
            scene,
            camera,
        }
    }

    pub fn get_camera(&self) -> &Camera {
        &self.camera
    }

    pub fn get_scene(&self) -> &Scene {
        &self.scene
    }

    pub fn get_camera_mut(&mut self) -> &mut Camera {
        &mut self.camera
    }

    pub fn get_scene_mut(&mut self) -> &mut Scene<'a> {
        &mut self.scene
    }
}
