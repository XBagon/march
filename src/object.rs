use crate::{render::Render, shader::Shader, types::*};

pub struct Object<'a> {
    pub transform: Transform3,
    pub render: Box<dyn Render + 'a>,
    pub shader: Box<dyn Shader + 'a>,
}

impl<'a> Object<'a> {
    pub fn new(transform: Transform3, render: impl Render + 'a, shader: impl Shader + 'a) -> Self {
        Object {
            transform,
            render: Box::new(render),
            shader: Box::new(shader),
        }
    }
}
