use crate::{
    ray_marcher::{MarchCollision, Ray},
    scene::Scene,
    shader::Shader,
    types::*,
};

#[derive(Clone)]
pub struct BWRotatedShader {
    rot: Rotation3,
}

impl BWRotatedShader {
    pub fn new(rot: Rotation3) -> Self {
        BWRotatedShader {
            rot,
        }
    }
}

impl Shader for BWRotatedShader {
    fn color(&self, ray: &Ray, march_col: &MarchCollision, _scene: &Scene) -> [u8; 4] {
        let dot = &march_col.normal.dot(&(-(self.rot * ray.direction)));
        let brightness = (dot.max(0.0) * 255.0) as u8;
        [brightness, brightness, brightness, 255]
    }
}
