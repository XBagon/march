pub mod bw_rotated_shader;

use crate::ray_marcher::{MarchCollision, Ray};
use crate::scene::Scene;

pub trait Shader: Sync {
    fn color(&self, ray: &Ray, march_col: &MarchCollision, scene: &Scene) -> [u8; 4];
}
