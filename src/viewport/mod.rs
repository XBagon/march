use piston_window::{clear, DrawState, Image, OpenGL, PistonWindow, Texture, TextureSettings, Window, WindowSettings};

use crate::{renderer::Renderer, world::World};

use image::{ImageBuffer, Rgba};

pub struct Viewport;

impl Viewport {
    pub fn start(mut renderer: Renderer, mut world: World, resolution_factor: f64) {
        let opengl = OpenGL::V3_2;

        const WIDTH: u32 = 500;
        const HEIGHT: u32 = 500;

        let mut window: PistonWindow = WindowSettings::new("Raymarcher", [WIDTH, HEIGHT]).exit_on_esc(true).graphics_api(opengl).build().unwrap();

        while let Some(e) = window.next() {
            let window_size = &window.draw_size();
            let (w, h) = (window_size.width.clone(), window_size.height.clone());

            world.get_camera_mut().set_resolution((w * resolution_factor) as u32, (h * resolution_factor) as u32);

            let render_image = renderer.render_to_image(&world);

            let render_texture = Texture::from_image(&mut window.create_texture_context(), &render_image, &TextureSettings::new()).unwrap();

            window.draw_2d(&e, |c, g, _| {
                let viewport_image = Image::new().rect([0.0, 0.0, w, h]);

                clear([0.5, 0.5, 0.5, 1.0], g);
                viewport_image.draw(&render_texture, &DrawState::default(), c.transform, g)
            });
        }
    }
}
