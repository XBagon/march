mod camera;
mod object;
mod ray_marcher;
mod render;
mod renderer;
mod scene;
mod shader;
mod viewport;
mod world;

mod types {
    pub type Vec3 = nalgebra::Vector3<f64>;
    pub type Point3 = nalgebra::Point3<f64>;
    pub type Point4 = nalgebra::Point4<f64>;
    pub type Isometry3 = nalgebra::Isometry3<f64>;
    pub type Perspective3 = nalgebra::Perspective3<f64>;
    pub type Rotation3 = nalgebra::Rotation3<f64>;
    pub type Transform3 = nalgebra::Transform3<f64>;
}

use crate::{
    camera::{Camera, Resolution},
    ray_marcher::RayMarcher,
    render::{sdf_cube::SdfCube, sphere::Sphere},
    renderer::RendererBuilder,
    scene::Scene,
    world::World,
};

use nalgebra::Vector3;

use shader::bw_rotated_shader::BWRotatedShader;

use crate::{
    object::Object,
    render::{subtraction::Subtraction, Render},
    shader::Shader,
};
use std::{fs, ops::Deref, path::Path};
use types::*;

fn main() {
    let dir = Path::new("renders");
    if !dir.exists() {
        fs::create_dir(dir).unwrap();
    }

    basic_test();
    subtraction_test();
}

fn basic_test() {
    let ray_marcher = RayMarcher::new(200, 1e-4);
    let shader = BWRotatedShader::new(Rotation3::new(Vec3::new(-0.3, 0.3, 0.0)));
    let mut scene = Scene::new();
    scene.add_object(Object::new(Transform3::identity(), Sphere::new(Point3::new(-2., 1., 5.), 1.), shader.clone()));
    scene.add_object(Object::new(
        Transform3::identity(),
        SdfCube::new(Point3::new(2., 0., 3.), Rotation3::new(Vec3::new(-0.2, 0.5, 0.3)), 0.5),
        shader.clone(),
    ));
    let camera = Camera::new(Isometry3::new(Vec3::new(0., 0., -5.), Vec3::zeros()), 45., 5., 100_000., Resolution::new(1000, 1000));

    let world = World::new(scene, camera);

    let renderer = RendererBuilder::new().build();

    viewport::Viewport::start(renderer, world, 0.25);
}

fn subtraction_test() {
    let ray_marcher = RayMarcher::new(200, 1e-4);
    let shader = BWRotatedShader::new(Rotation3::new(Vec3::new(-0.3, 0.3, 0.0)));
    let mut scene = Scene::new();
    scene.add_object(Object::new(
        Transform3::identity(),
        Subtraction::new(Sphere::new(Point3::new(1.3, 0.6, 3.4), 1.), SdfCube::new(Point3::new(0., 1.3, 3.4), Rotation3::new(Vec3::new(0., 30., 45.)), 1.2)),
        shader.clone(),
    ));

    let camera = Camera::new(Isometry3::new(Vec3::new(0., 0., -5.), Vec3::zeros()), 45., 5., 100_000., Resolution::new(1000, 1000));

    let renderer = RendererBuilder::new().build();

    let world = World::new(scene, camera);

    viewport::Viewport::start(renderer, world, 0.25);
}
