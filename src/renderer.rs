use crate::{
    ray_marcher::{Ray, RayMarcher},
    scene::Scene,
    types::*,
    world::World,
};
use image::{ImageBuffer, Rgba};
use rayon::{prelude::*, slice::ParallelSliceMut};
use std::sync::{
    atomic::{AtomicBool, Ordering},
    Arc,
};

pub struct Renderer {
    raymarcher: RayMarcher,
    thread_count: usize,
    chunk_size: Option<usize>,
    pub should_refresh: Arc<AtomicBool>,
}

#[derive(Clone)]
pub struct RendererBuilder {
    raymarcher: Option<RayMarcher>,
    thread_count: usize,
    chunk_size: Option<usize>,
}

impl RendererBuilder {
    pub fn new() -> Self {
        RendererBuilder {
            raymarcher: None,
            thread_count: 4, // TODO: check amount of available cores
            chunk_size: None,
        }
    }

    pub fn build(self) -> Renderer {
        let raymarcher = self.raymarcher.unwrap_or_default();

        Renderer {
            raymarcher,
            thread_count: self.thread_count,
            chunk_size: self.chunk_size,
            should_refresh: Arc::new(AtomicBool::new(false)),
        }
    }

    pub fn with_raymarcher(mut self, raymarcher: RayMarcher) -> Self {
        self.raymarcher = Some(raymarcher);
        self
    }

    pub fn thread_num(mut self, threads: usize) -> Self {
        self.thread_count = threads;
        self
    }

    pub fn chunked(mut self, chunk_size: usize) -> Self {
        self.chunk_size = Some(chunk_size);
        self
    }
}

impl Renderer {
    pub fn render_to_image(&mut self, world: &World) -> ImageBuffer<Rgba<u8>, Vec<u8>> {
        let (camera, scene) = (world.get_camera(), world.get_scene());

        let resolution = camera.get_resolution();
        let perspective = camera.compute_perspective();

        let mut pixel_buffer = vec![0u8; resolution.x as usize * resolution.y as usize * 4];
        let projection = (perspective.as_matrix() * camera.isometry.to_homogeneous()).try_inverse().unwrap();
        pixel_buffer.par_chunks_mut(4).enumerate().try_for_each(|(i, color)| {
            let x = i as u32 % resolution.x;
            let y = i as u32 / resolution.x;
            let i = ((x as f64 / resolution.x as f64) - 0.5) * 2.0;
            let j = ((y as f64 / resolution.y as f64) - 0.5) * 2.0;
            let p = Point4::new(i, j, -1., 1.);
            let target = Point3::from_homogeneous((projection * p).coords).unwrap();
            let eye = camera.isometry * Point3::origin();
            let ray = Ray::new(eye, (target - eye).normalize());

            let march_result = self.raymarcher.march(&ray, scene);

            if let Some(march_collision) = march_result {
                let to_color = march_collision.object.shader.color(&ray, &march_collision, scene);

                color[0] = to_color[0];
                color[1] = to_color[1];
                color[2] = to_color[2];
                color[3] = to_color[3];
            }

            if self.should_refresh.load(Ordering::Relaxed) {
                None
            } else {
                Some(())
            }
        });
        self.should_refresh.store(false, Ordering::Relaxed);
        ImageBuffer::from_raw(resolution.x, resolution.y, pixel_buffer).unwrap()
    }
}
